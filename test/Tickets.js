contract('Tickets', function(accounts) {
	console.log(accounts);
	var owner_account = accounts[0];
  var sender_account = accounts[1];


  it("Initial Tickets settings should match", function(done) {
  	
  	Tickets.new({from: owner_account}).then(
  		function(Tickets) {
  			Tickets.quota.call().then(
  				function(quota) { 
  					assert.equal(quota, 100, "Quota doesn't match!"); 
  			}).then(
  				function() { 
  					return Tickets.numRegistrants.call(); 
  			}).then(
  				function(num) { 
  					assert.equal(num, 0, "Registrants doesn't match!");
  					return Tickets.organizer.call();
  			}).then(
  				function(organizer) { 
  					assert.equal(organizer, owner_account, "Owner doesn't match!");
  					done();
  			}).catch(done);
  	}).catch(done);
  });

  it("Should update quota", function(done) {
  	
  	Tickets.new({from: owner_account}).then(
  		function(Tickets) {
  			Tickets.quota.call().then(
  				function(quota) { 
  					assert.equal(quota, 100, "Quota doesn't match!"); 
  			}).then(
  				function() { 
  					return Tickets.changeQuota(300);
  			}).then(
  				function() { 
  					return Tickets.quota.call()
  			}).then(
  				function(quota) { 
  					assert.equal(quota, 300, "New quota is not correct!");
  					done();
  			}).catch(done);
  	}).catch(done);
  });


  it("Should let you buy a ticket", function(done) {

  	Tickets.new({ from: accounts[0] }).then(
  		function(Tickets) {

        var ticketPrice = web3.toWei(.05, 'ether');
        var initialBalance = web3.eth.getBalance(Tickets.address).toNumber();  

  			Tickets.buyTicket({ from: accounts[1], value: ticketPrice }).then(
          function() {
  					var newBalance = web3.eth.getBalance(Tickets.address).toNumber();
            var difference = newBalance - initialBalance;
  					assert.equal(difference, ticketPrice, "Difference should be what was sent");
  					return Tickets.numRegistrants.call(); 
  			}).then(
  				function(num) { 
  					assert.equal(num, 1, "there should be 1 registrant");
  					return Tickets.registrantsPaid.call(sender_account);
  			}).then(
  				function(amount) {
  					assert.equal(amount.toNumber(), ticketPrice, "Sender's paid but is not listed as paying");	
  					return web3.eth.getBalance(Tickets.address);
  			}).then(
  				function(bal) {
            assert.equal(bal.toNumber(), ticketPrice, "Final balance mismatch");
  					done();
  			}).catch(done);
  	}).catch(done);
  });

  it("Should issue a refund by owner only", function(done) {
    
    Tickets.new({ from: accounts[0] }).then(
      function(Tickets) {

        var ticketPrice = web3.toWei(.05, 'ether');
        var initialBalance = web3.eth.getBalance(Tickets.address).toNumber(); 

        Tickets.buyTicket({ from: accounts[1], value: ticketPrice }).then(
          function() {
            var newBalance = web3.eth.getBalance(Tickets.address).toNumber();
            var difference = newBalance - initialBalance;
            assert.equal(difference, ticketPrice, "Difference should be what was sent");

            // Now try to issue refund as second user - should fail
            return Tickets.refundTicket(accounts[1], ticketPrice, {from: accounts[1]});
        }).then(
          function() {  
            var balance = web3.eth.getBalance(Tickets.address);
            assert.equal(balance, ticketPrice, "Balance should be unchanged");
            // Now try to issue refund as organizer/owner
            return Tickets.refundTicket(accounts[1], ticketPrice, {from: accounts[0]});
        }).then(
          function() {
            var postRefundBalance = web3.eth.getBalance(Tickets.address).toNumber();
            assert.equal(postRefundBalance, initialBalance, "Balance should be initial balance");
            done();
        }).catch(done);
      }).catch(done);
    });

});

