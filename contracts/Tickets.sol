pragma solidity ^0.4.0;
contract Tickets {  // can be killed, so the owner gets sent the money in the end

	address public organizer;		//组织者的钱包地址，在构造函数中初始化
	//以下声明也可写成address registrantsPaid[]，保存参加者的付款数量以便退款时使用
	mapping (address => uint) public registrantsPaid; //uint. 无符号整型
	uint public numRegistrants;
	uint public quota;
	uint public price;

    //事件怎么具体实现？？？？
	event Deposit(address _from, uint _amount); // 充值事件 so you can log the event
	event Refund(address _to, uint _amount); // 退款事件 so you can log the event

	function Tickets() {
		organizer = msg.sender;		//初始化组织者organizer；msg.sender和msg.value是系统自带的？？？
		quota = 100;                //配额初始化（这块可优化！！！）
		numRegistrants = 0;	//已卖出票数
		price = 1;	//初始化票价
	}

	function buyTicket() payable public {
		if (numRegistrants >= quota) {
			throw; // throw ensures funds will be returned
		}
		registrantsPaid[msg.sender] = price;	//转账交易，包含发送人和交易价值
		numRegistrants++;
		Deposit(msg.sender, msg.value);
	}

	function changeQuota(uint newquota) public {
		if (msg.sender != organizer) { return; }
		quota = newquota;
	}

	function changePrice(uint newprice) public {
		if (msg.sender != organizer) { return; }
		price = newprice;
	}

	/*
	function refundTicket(address recipient) public {
		if (msg.sender != organizer) { return; }
		uint amount = registrantsPaid[recipient];
		if (amount != 0) {
			address myAddress = this;		//this表示部署的合约在区块链上的地址（与组织者地址不同）
			registrantsPaid[recipient] = 2;
			if (myAddress.balance >= amount) {
			    if(!recipient.send(amount)) {
				registrantsPaid[recipient] = 1;
                    		throw;
            		    }		
				Refund(recipient, amount);
				registrantsPaid[recipient] = 0;
				numRegistrants--;
			}
		}
		return;
	}
	*/
	
	function refundTicket(address recipient, uint amount) public {
		if (msg.sender != organizer) { return; }
		if (registrantsPaid[recipient] == amount) { 
			address myAddress = this;		//this表示部署的合约在区块链上的地址（与组织者地址不同）
			if (myAddress.balance >= amount) { 
				if(!recipient.send(amount)){
					throw;
				}
				Refund(recipient, amount);
				registrantsPaid[recipient] = 0;
				numRegistrants--;
			}
		}
		return;
	}

  //将合约地址中的资金转给组织者地址
	function destroy() {
		if (msg.sender == organizer) { // without this funds could be locked in the contract forever!
			suicide(organizer);		//suicide是solidity提供的关键字
		}
	}
}
